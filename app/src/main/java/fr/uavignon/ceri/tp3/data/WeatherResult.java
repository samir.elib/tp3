package fr.uavignon.ceri.tp3.data;

public class WeatherResult {

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){

        cityInfo.setTemperature(weatherInfo.main.temp);

        cityInfo.setWindSpeed(weatherInfo.wind.speed);

        cityInfo.setCloudiness(weatherInfo.cloud.all);

        cityInfo.setHumidity(weatherInfo.main.humidity);

        cityInfo.setIcon(weatherInfo.weather.icon);




    }
}
