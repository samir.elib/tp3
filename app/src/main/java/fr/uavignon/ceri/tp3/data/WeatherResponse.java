package fr.uavignon.ceri.tp3.data;

public class WeatherResponse {
    public final Weather weather = null;
    public final Main main = null;
    public final Wind wind = null;
    public final Cloud cloud = null;


    public static class Main{ int temp, humidity; }

    public static class Weather{ String icon; }

    public static class Wind{ int speed; }

    public static class Cloud { int all; }
}
